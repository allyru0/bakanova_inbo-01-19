from calculator import convert
def test_convert():
    assert convert(4, 'мм', 'мм') == 4.0
    assert convert(4, 'мм', 'см') == 0.4
    assert convert(4, 'мм', 'дм') == 0.04
    assert convert(4, 'мм', 'м') == 0.004
    assert convert(1000, 'мм', 'км') == 0.001

    assert convert(4, 'см', 'мм') == 40.0
    assert convert(4, 'см', 'см') == 4.0
    assert convert(4, 'см', 'дм') == 0.4
    assert convert(4, 'см', 'м') == 0.04
    assert convert(1000, 'см', 'км') == 0.01

    assert convert(4, 'дм', 'мм') == 400.0
    assert convert(4, 'дм', 'см') == 40.0
    assert convert(4, 'дм', 'дм') == 4.0
    assert convert(4, 'дм', 'м') == 0.4
    assert convert(4, 'дм', 'км') == 0.0004

    assert convert(4, 'м', 'мм') == 4000.0
    assert convert(4, 'м', 'см') == 400.0
    assert convert(4, 'м', 'дм') == 40.0
    assert convert(4, 'м', 'м') == 4.0
    assert convert(4, 'м', 'км') == 0.004

    assert convert(4, 'км', 'мм') ==4000000.0
    assert convert(4, 'км', 'см') ==400000.0
    assert convert(4, 'км', 'дм') ==40000.0
    assert convert(4, 'км', 'м') ==4000.0
    assert convert(4, 'км', 'км') ==4.0

    
